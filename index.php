<?php
    include 'Controllers/BuletinController.php';

    $buletinController = new BuletinController();
    $buletins          = $buletinController->index();
    
    if ($_SERVER['REQUEST_METHOD']=='POST') {
        $buletinController->store();
    }
?>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title> Buletin Board</title>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
        <link href="Assets/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <style>
            input[type="text"]::placeholder {
                color: orangered;
            }
            #body::placeholder {
                color: orangered;
            }
        </style>
        <div class="container" align="center">
            <div style="margin-top: 10px" class="container border border-dark">
                <?php
                    if(isset($buletinController->errors)) {
                        foreach($buletinController->errors as $error) {
                            echo('<span style="color:red">'. $error .'</span><br>');
                        }
                    }
                ?>
                <br>
                <h1><u>Bulletin Board</u></h1>
                <div class="row">
                    <div class="col-12">
                        <form onsubmit="return validateForm()" name="Form" style="margin:0 auto" action="<?= $_SERVER["PHP_SELF"];?>" method="post">
                            <div class="form-group">
                                <input id="title" class="form-control input-sm" type="text" name="title" placeholder=""/>
                            </div>
                            <div class="form-group">
                                <textarea id="body" class="form-control" name="body" placeholder="" rows="6" cols="50"></textarea>
                            </div>
                            <button type="submit" class="btn btn-sm btn-primary"> Submit Buletin</button>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                <?php
                    foreach($buletins['data'] as $buletin):
                ?>
                    <div align="left" class="col-12">
                        <h3><?= htmlspecialchars($buletin['title']) ?></h3>
                        <p><?= htmlspecialchars($buletin['body']) ?></p>
                        <p align="right"><?= date_format(date_create($buletin['created_at']), "d-m-Y H:i"); ?></p>
                        <hr>
                    </div>
                <?php
                    endforeach
                ?>
                </div>
                <div style="margin-left:-210px;" class="pagination">
                    <nav style="margin:0 auto;">
                        <ul style="position: fixed;bottom: 0;" class="pagination">
                        <?php if($buletins['current_page'] > 1): ?>
                            <li><a <?php echo "href='?page=" . $buletins['prev'] . "'" ?> class="p-2 mx-2 border">&laquo;</a></li>
                        <?php endif ?>
                        <?php
                            foreach($buletins['pager'] as $page):
                        ?>
                            <li class="<?php if ($buletins['current_page'] == $page) { echo "active"; } ?>"> <a <?php echo "href='?page=$page'" ?> class="p-2 mx-2 border"><?php echo $page ?></a> </li>
                        <?php
                            endforeach
                        ?>
                        <?php if($buletins['current_page'] < $buletins['total_page'] ): ?>
                            <li><a <?= "href='?page=" . $buletins['next'] . "'" ?> class="p-2 mx-2 border">&raquo;</a></li>
                        <?php endif ?>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </body>
    <footer>
        <script>
            function validateForm() {
                var title = document.forms["Form"]["title"].value;
                var body = document.forms["Form"]["body"].value;
                if (title == null || title == "" && body == null || body == "") {
                    document.getElementsByName('body')[0].placeholder='must be filled in';
                    document.getElementsByName('title')[0].placeholder='must be filled in';
                    return false;
                }else if (body == null || body == "") {
                    document.getElementsByName('body')[0].placeholder='must be filled in';
                    return false;
                }else if(title == null || title == ""){
                    document.getElementsByName('title')[0].placeholder='must be filled in';
                    return false;
                }
            }
        </script>
    </footer>
</html>