<?php
include 'Model/Buletin.php';
include 'Controllers.php';

class BuletinController extends Controllers {

    public function index()
    {
        $buletin = new Buletin();

        $paginate = $buletin->orderByDateCreated()->pagination();

        return $paginate;
    }

    public function store()
    {
        $message = [
            'title.length:10-32' => 'panjang title harus 10 sampai 32',
            'body.length:10-200' => 'panjang content harus 10 sampai 200'
        ];

        $this->inputValidation([
            'title' => 'length:10-32',
            'body'  => 'length:10-200',
        ], $message);

        if (empty($this->errors)) {
    
            date_default_timezone_set('Asia/Makassar');
            $buletin                      = new Buletin();
            $buletin->input['title']      = $_POST['title'];
            $buletin->input['body']       = $_POST['body'];
            $buletin->input['created_at'] = date('Y-m-d H:i:s');

            $buletin->save();

            header("location:index.php");
        }          
    }
}
?>