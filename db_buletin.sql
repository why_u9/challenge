/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 8.0.20 : Database - db_buletin
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_buletin` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `db_buletin`;

/*Table structure for table `buletins` */

DROP TABLE IF EXISTS `buletins`;

CREATE TABLE `buletins` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(32) DEFAULT NULL,
  `body` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `buletins` */

insert  into `buletins`(`id`,`title`,`body`,`created_at`) values 
(62,'xsanx asj','saxnkaskxs','2021-09-07 04:09:00'),
(63,',sxnsanlkx','sajkxnkassxlk','2021-09-07 04:09:00'),
(64,'s,mx as xm','xasjnxankxs','2021-09-07 04:09:00'),
(65,'skanskldnklas','askdmlmsl;defmw;','2021-09-07 04:09:00'),
(66,'xxxxxxxxxx','xxxxxxxxxx','2021-09-07 04:09:00'),
(67,'xxxxxxxxxxxx','xxxxxxxxxxxxxxxx','2021-09-07 04:09:00'),
(68,'xxxxxxxxxxxxxxxxxxx','xxxxxxxxxxxxxxxxxxxx','2021-09-07 04:09:00'),
(69,'akakakkkkkkkkkk','akkkkkkkkkk','2021-09-07 05:09:00'),
(70,'nssnsnaksnxksakx','skxksxkskxksnknsk','2021-09-07 05:09:00'),
(71,'askxnksnkx','ksxnkanskxn','2021-09-07 05:09:00'),
(72,'xaasasxasxasxas','saxsaxasxsaxasxa','2021-09-07 05:09:00'),
(73,'xjasbkxjakjsndjkndekw','kjwendkwendwnedooe','2021-09-07 05:09:00'),
(74,'lkmlcmsldmf;s;ld','lkasdmlkasldklka','2021-09-07 05:09:00'),
(75,'lkmlcmsldmf;s;ld','lkmlcmsldmf;s;ld','2021-09-07 05:09:00'),
(76,'lkmlcmsldmf;s;ld','lkmlcmsldmf;s;ld','2021-09-07 05:09:00'),
(77,'lkmlcmsldmf;s;ld','lkmlcmsldmf;s;ld','2021-09-07 05:09:00'),
(78,'lkmlcmsldmf;s;ld','lkmlcmsldmf;s;ld','2021-09-07 05:09:00'),
(79,'lnlkllknlknlj','lnlkllknlknlj','2021-09-07 05:09:00'),
(80,'xxjjjjjjjjjjjjjjjjj','lkasndkjedfw','2021-09-07 06:09:00'),
(81,'lsdnfknskdnvlknlrdnlernl','sdnflkndlkfrnk','2021-09-07 06:09:00'),
(82,'           ','           ','2021-09-07 06:09:00'),
(83,',asndkankjdnalkn','asjkndlkanwlkdnkl','2021-09-07 09:09:00'),
(84,'jdsandlalsks','ljasndlkanlk','2021-09-07 10:09:00'),
(85,'ajxasaxjaxjs','jxsnxkanskxnas\r\n','2021-09-07 10:09:00'),
(86,'nXz,zmxnaskncdn','dmcmslmlsdlcmsl','2021-09-07 10:09:00'),
(87,'kdsancnnsdf','knckankcank','2021-09-07 10:09:00'),
(88,'cnnsncjnsdjncj','cmsmcmslmlm\r\n','2021-09-07 11:09:00'),
(89,'aaaaaaaaaaaaaaaaaa','aaaaaaaaaaaaaaaaaa','2021-09-08 08:09:00'),
(90,'jsndklkenwd','sdjnfkwekwnkw','2021-09-08 09:09:00'),
(91,'sd cjwekjwne','kjdnfkewnrekw','2021-09-08 09:09:00'),
(92,'lkdnlwelfnlwkenf',',sdnflksmldkfwlem','2021-09-08 09:09:00'),
(93,'ndskfnwkenlk','dwenldkwnlkw','2021-09-08 09:09:00'),
(94,'c mac askxaksk','mcankankdnksn','2021-09-08 09:09:00'),
(95,'kcnaknaknkac','ckasxkaksmksd','2021-09-08 09:09:00'),
(96,'ckacaskcakmskmdk','kcanscaksckkskcmkm','2021-09-08 09:09:00'),
(97,'cnksa cka skc ksa','cm askc a scas ka','2021-09-08 09:09:00'),
(98,'c ma a sc am skc','nxas ka skx ksaks','2021-09-08 09:09:00'),
(99,'c msd kc sdkc','ckds c skd ckds','2021-09-08 09:09:00'),
(100,'ck dk ksd cs','c kd aas cas cka','2021-09-08 09:09:00'),
(101,'cccccccccccccc','cccccccccccccc','2021-09-08 09:09:00'),
(102,'ccccccccccccccccccccccc','ccccccccccccccccccccccccc','2021-09-08 09:09:00'),
(103,'ccccccccccccccccccccccccccccc','ccccccccccccccccccccccccccc','2021-09-08 09:09:00'),
(104,'ccccccccccccccccccc','ccccccccccccccccccc','2021-09-08 09:09:00'),
(105,'ccccccccccccccc','cccccccccccccc','2021-09-08 09:09:00'),
(106,'ccccccccccccccccccccccccc','ccccccccccccccccccccccccc','2021-09-08 09:09:00'),
(107,'cccccccccccccccccccc','cccccccccccccccccccc','2021-09-08 09:09:00'),
(108,'ccccccccccccccccccccc','cccccccccccccccccccc','2021-09-08 09:09:00'),
(109,'cccccccccccccccccccccccccccccc','ccccccccccccccccccccccccccccc','2021-09-08 09:09:00'),
(110,'ccccccccccccccccccccc','ccccccccccccccccccccc','2021-09-08 09:09:00'),
(111,' mksfklwkfmwkefm','fkerkfkermkfmerkfmrk','2021-09-08 09:09:00'),
(112,'cm m cmd mc smdc m','c m mcmsd cmm cmc ','2021-09-08 09:09:00'),
(113,'f;kwldmfwlkmew','ksdnlfwekflwkef','2021-09-08 10:09:00'),
(114,'nldkncjsdnjf','kfndknfknk','2021-09-08 10:09:00'),
(115,'cssxxxxxxxx','xxxxxxxxxxx','2021-09-08 10:09:00'),
(116,'lnnjndjnaskd','danknkkskws','2021-09-08 01:09:00'),
(117,' cm mc mc m cm','c m dms dmmdc ','2021-09-08 01:09:00'),
(118,'nxnanslcldsaknlck','kcnxsdlknksdlk','2021-09-08 01:09:00'),
(119,' xxxxxxxxxxxxxxxxxxxx','xxxxxxxxxxxxxxxxx','2021-09-08 01:09:00'),
(120,'kdjnfwieoil','djwioejfwnoeifnoi','2021-09-08 02:09:00'),
(121,'lfdmvlkmdflmvl','dlkfmglkdmf','2021-09-08 02:09:00'),
(122,'nfdlgrm;gtrni','jfdkgperkpogrkpo','2021-09-08 02:09:00'),
(123,'uhiuhiuhihu','jojopiokoo','2021-09-08 02:09:00'),
(124,'nsdnwmewkmkd','kdjkwmekdkwe','2021-09-08 03:09:00'),
(125,'nnnnnnnnnnn','jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj','2021-09-08 03:09:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
