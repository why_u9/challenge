<?php
include 'QueryBuilder/QueryBuilder.php';

class Pagination extends QueryBuilder {

    public $start           = 0;
    public $limit           = 10;
    public $currentPage     = 1;
    public $pagesCount      = 0;
    public $paginationQuery = '';
    public $previous;
    public $next;

    public function __construct()
    {
        if (isset($_GET['page'])) {
            $this->currentPage = $_GET['page'];
            $this->start       = $this->currentPage * $this->limit - $this->limit;
        }

        $this->previous = $this->currentPage - 1;
        $this->next     = $this->currentPage + 1;
    }

    public function setPaginationQuery($query)
    {
        $this->paginationQuery = $query;
        $this->pagesCount      = ceil($this->countData() / $this->limit);
    }

    public function countData()
    {
        $paginationQuery = clone $this->paginationQuery;
        $paginationQuery->select('COUNT(*) as total');

        $data = mysqli_fetch_assoc(mysqli_query($this->connect(), $paginationQuery));

        return $data['total'];
    }

    public function showPager()
    {
        if ($this->pagesCount > 5) {
            $firstPagination = $this->currentPage - 2;
            $lastPagination  = $this->currentPage + 2;
        } else {
            $firstPagination = 1;
            $lastPagination  = $this->pagesCount;
        }

        if ($firstPagination == 0) {
            $firstPagination  = 1;
            $lastPagination  += 1;
        } else if ($firstPagination < 0) {
            $firstPagination  = 1;
            $lastPagination  += 2;
        }

        if ($lastPagination  == $this->pagesCount + 2) {
            $lastPagination   = $this->pagesCount;
            $firstPagination -= 2;
        } else if($lastPagination == $this->pagesCount + 1) {
            $lastPagination   = $this->pagesCount;
            $firstPagination -= 1;
        }

        $pager = [];

        for ($page = $firstPagination; $page <= $lastPagination; $page++) {
            $pager[] = $page;
        }

        return $pager;
    }

    public function getPaginationData()
    {
        $paginationQuery = clone $this->paginationQuery;
        $query           = $paginationQuery->limit($this->start, $this->limit);

        return mysqli_query($this->connect(), $query);
    }
}
?>