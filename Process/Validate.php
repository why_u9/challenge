<?php
trait Validate {

    public $errors = [];

    public function inputValidation($fields, $messages)
    {
        foreach($fields as $fieldName => $requirements) {
            $this->fetchRequirements($fieldName, $requirements, $messages, '|');
        }
    }

    private function fetchRequirements($fieldName, $requirements, $messages, $delimiter)
    {
        foreach(explode($delimiter, $requirements) as $requirement) {
            $this->required($fieldName, $requirement, $messages);
            $this->length($fieldName, $requirement, $messages);
        }
    }

    private function required($fieldName, $requirement, $messages)
    {
        if ($requirement == 'required' && empty($_POST[$fieldName])) {
            $defaultMessage = $fieldName . ' must be filled in';
            $this->setErrorMessage($messages, $fieldName, $requirement, $defaultMessage);
        }
    }

    private function length($fieldName, $requirement, $messages)
    {
        if (preg_match('/length:[0-9]{1,}-[0-9]{1,}/', $requirement) == 1 && 
            !empty($_POST[$fieldName]) && 
            strlen($_POST[$fieldName]) < str_replace("length:", "", explode('-', $requirement)[0]) 
            || 
            preg_match('/length:[0-9]{1,}-[0-9]{1,}/', $requirement) == 1 && 
            !empty($_POST[$fieldName]) && 
            strlen($_POST[$fieldName]) > explode('-', $requirement)[1]) {

            $min            = str_replace("length:", "", explode('-', $requirement)[0]);
            $max            = str_replace("length:", "", explode('-', $requirement)[1]);
            $defaultMessage = 'Your '. $fieldName . ' must be ' . $min . ' to ' . $max . ' characters long';

            $this->setErrorMessage($messages, $fieldName, $requirement, $defaultMessage);
        }
    }

    private function setErrorMessage($messages, $fieldName, $requirement, $defaultMessage)
    {
        if (isset($messages[$fieldName . '.' . $requirement])) {
            $this->errors[] = $messages[$fieldName . '.' . $requirement];
        } else {
            $this->errors[] = $defaultMessage; 
        }
    }
}
?>