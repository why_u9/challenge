<?php
include 'Process/Pagination.php';

class Models extends Pagination {

    public $query;
    protected $table;
    protected $fillable;
    public $input;

    public function all()
    {
        $this->query = (new QueryBuilder())->select('*')->from($this->table);

        return $this;
    }

    public function orderByDateCreated()
    {
        $this->query = (new QueryBuilder())->select('*')->from($this->table)->orderBy('created_at', 'DESC');

        return $this;
    }

    public function get()
    {
        return mysqli_query($this->connect(), $this->query);
    }

    public function pagination()
    {
        $pagination = new Pagination();
        $pagination->setPaginationQuery($this->query);

        return [
            "total_page"   => $pagination->pagesCount,
            "current_page" => $pagination->currentPage,
            "next"         => $pagination->next,
            "prev"         => $pagination->previous,
            "pager"        => $pagination->showPager(),
            "data"         => $pagination->getPaginationData()
        ];
    }

    public function save()
    {
        $query = 'INSERT INTO ' . $this->table . '(' . implode(', ', array_keys($this->fillable)) . ') VALUES("' . implode('" , "', $this->input) . '")';
        
        if ($this->connect()->query($query) === TRUE) {
            return "Data Berhasil Diinput";
        } else {
            return "Error: " . $query . "<br>" . $this->connect()->error;
        }
    }
}
?>