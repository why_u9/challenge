<?php
include 'Env.php';

class QueryBuilder extends Connection {

    private $value     = [];
    private $condition = [];
    private $from      = [];
    private $order     = '';
    private $limit     = '';

    public function __toString()
    {
        $where   = $this->condition  == [] ? '' : ' WHERE ' . implode(' AND ', $this->condition);
        $orderBy = $this->order      == '' ? '' : ' ORDER BY ' . $this->order;
        $limit   = $this->limit      == '' ? '' : ' LIMIT ' . $this->limit;

        return 'SELECT ' . implode(', ', $this->value). ' FROM ' . implode(', ', $this->from). $orderBy. $where. $limit;
    }

    public function select(string ...$select)
    {
        $this->value = $select;

        return $this;
    }

    public function where(string ...$where)
    {
        foreach($where as $arg) {
            $this->condition[] = $arg;
        }

        return $this;
    }

    public function from(string $table, ?string $alias = null)
    {
        if ($alias == null) {
            $this->from[] = $table;
        } else {
            $this->from[] = "${table} AS ${alias}";
        }

        return $this;
    }

    public function orderBy(string $fieldName, string $orderType)
    {
        $this->order = $fieldName . ' ' . $orderType;

        return $this;
    }

    public function limit(int $limitFirst, int $limitSize)
    {
        $this->limit = $limitFirst . ', ' . $limitSize;

        return $this;
    }
}
?>